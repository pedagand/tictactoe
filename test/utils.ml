open Tictactoe.Engine
open Tictactoe.Arena

let player =
  Alcotest.testable
    (Fmt.of_to_string (Format.asprintf "%a" pp_player))
    equal_player

let board =
  Alcotest.testable
    (Fmt.of_to_string (Format.asprintf "%a" pp_board))
    equal_board

let hpos =
  Alcotest.testable (Fmt.of_to_string (Format.asprintf "%a" pp_hpos)) equal_hpos

let vpos =
  Alcotest.testable (Fmt.of_to_string (Format.asprintf "%a" pp_vpos)) equal_vpos

let endplay =
  Alcotest.testable
    (Fmt.of_to_string (Format.asprintf "%a" pp_endplay))
    equal_endplay

let trace =
  Alcotest.testable
    (Fmt.of_to_string (Format.asprintf "%a" pp_trace))
    equal_trace

let example1 =
  init
    [
      [ None; Some X; Some O ];
      [ None; Some O; Some X ];
      [ Some X; Some O; Some O ];
    ]

let example2 =
  init
    [
      [ None; Some X; Some O ];
      [ Some O; Some O; Some X ];
      [ Some X; Some O; Some O ];
    ]

let example3 =
  init
    [
      [ None; Some X; Some O ];
      [ Some X; Some O; Some X ];
      [ Some X; Some O; Some O ];
    ]

let example4 =
  init
    [
      [ Some O; Some X; Some O ];
      [ Some X; Some O; Some X ];
      [ Some X; Some O; Some O ];
    ]
