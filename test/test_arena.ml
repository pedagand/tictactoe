open Tictactoe.Engine
open Tictactoe.Arena
open Utils

let trace1 = []

let trace2 =
  [
    (Pos.(h 0), Pos.(v 0));
    (Pos.(h 1), Pos.(v 0));
    (Pos.(h 0), Pos.(v 1));
    (Pos.(h 0), Pos.(v 2));
    (Pos.(h 1), Pos.(v 1));
    (Pos.(h 1), Pos.(v 2));
    (Pos.(h 2), Pos.(v 2));
  ]

let test_trace t e () =
  let res = Lwt_main.run (arena (players_trace t)) in
  Alcotest.(check trace) "same result" t res.trace;
  Alcotest.(check endplay) "same result" e res.endplay

let () =
  let open Alcotest in
  run "Arena"
    [
      ( "arena",
        [
          test_case "arena-win" `Quick (fun () ->
              Alcotest.(check endplay)
                "same result" (Win O)
                (Lwt_main.run
                   (arena ~init_player:X ~init_board:example4
                      (Tictactoe.Arena.pair ~x:player_giveup ~o:player_giveup)))
                  .endplay);
          test_case "arena-giveup" `Quick (fun () ->
              Alcotest.(check endplay)
                "same result" (Giveup X)
                (Lwt_main.run
                   (arena ~init_player:X ~init_board:example3
                      (Tictactoe.Arena.pair ~x:player_giveup ~o:player_giveup)))
                  .endplay);
          test_case "arena-trace1" `Quick (test_trace trace1 (Giveup X));
          test_case "arena-trace2" `Quick (test_trace trace2 (Win X));
        ] );
    ]
