open Tictactoe.Engine
open Utils

let example1_get res i j () =
  Alcotest.(check (option player))
    "same result" res
    (get example1 Pos.(h i) Pos.(v j))

let () =
  let open Alcotest in
  run "Engine"
    [
      ( "init-get",
        [
          test_case "example1-get-0-0" `Quick (example1_get None 0 0);
          test_case "example1-get-0-1" `Quick (example1_get (Some X) 0 1);
          test_case "example1-get-0-2" `Quick (example1_get (Some O) 0 2);
          test_case "example1-get-1-0" `Quick (example1_get None 1 0);
          test_case "example1-get-1-1" `Quick (example1_get (Some O) 1 1);
          test_case "example1-get-1-2" `Quick (example1_get (Some X) 1 2);
          test_case "example1-get-2-0" `Quick (example1_get (Some X) 2 0);
          test_case "example1-get-2-1" `Quick (example1_get (Some O) 2 1);
          test_case "example1-get-2-2" `Quick (example1_get (Some O) 2 2);
        ] );
      ( "pp",
        [
          test_case "example1-pp-h" `Quick (fun () ->
              Alcotest.(check string)
                "same result" ".XO.OXXOO"
                (Format.asprintf "@[<h>%a@]" pp_board example1));
          test_case "example1-pp-v" `Quick (fun () ->
              Alcotest.(check string)
                "same result" ".XO\n.OX\nXOO\n"
                (Format.asprintf "@[<v>%a@]" pp_board example1));
        ] );
      ( "set",
        [
          test_case "example1-set-fail" `Quick (fun () ->
              Alcotest.(check_raises) "Invalid move" Invalid_move (fun () ->
                  ignore (set example1 Pos.(h 0) Pos.(v 1) X)));
          test_case "example1-set-example2" `Quick (fun () ->
              Alcotest.(check board)
                "same result" example2
                (set example1 Pos.(h 1) Pos.(v 0) O));
          test_case "example1-set-example3" `Quick (fun () ->
              Alcotest.(check board)
                "same result" example3
                (set example1 Pos.(h 1) Pos.(v 0) X));
          test_case "example3-set-example4" `Quick (fun () ->
              Alcotest.(check board)
                "same result" example4
                (set example3 Pos.(h 0) Pos.(v 0) O));
        ] );
      ( "free",
        [
          test_case "example1-free" `Quick (fun () ->
              Alcotest.(check (list (pair hpos vpos)))
                "same result"
                [ (Pos.(h 0), Pos.(v 0)); (Pos.(h 1), Pos.(v 0)) ]
                (free example1));
          test_case "example2-free" `Quick (fun () ->
              Alcotest.(check (list (pair hpos vpos)))
                "same result"
                [ (Pos.(h 0), Pos.(v 0)) ]
                (free example2));
        ] );
      ( "win",
        [
          test_case "example1-not-win-X" `Quick (fun () ->
              Alcotest.(check bool) "same result" false (win example1 X));
          test_case "example1-not-win-O" `Quick (fun () ->
              Alcotest.(check bool) "same result" false (win example1 O));
          test_case "example3-not-win-O" `Quick (fun () ->
              Alcotest.(check bool) "same result" false (win example3 O));
          test_case "example4-win-O" `Quick (fun () ->
              Alcotest.(check bool) "same result" true (win example4 O));
          test_case "example4-not-win-X" `Quick (fun () ->
              Alcotest.(check bool) "same result" false (win example4 X));
        ] );
    ]
