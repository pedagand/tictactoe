open Tictactoe.Arena

let () =
  Format.open_vbox 0;
  let result =
    Lwt_main.run (arena (pair ~x:(player_teletype X) ~o:player_random))
  in
  Format.printf "Game ends with %a@," pp_endplay result.endplay;
  Format.printf "Trace: @[<v>%a@]" pp_trace result.trace;
  Format.close_box ();
  Format.printf "@."
