open Engine

let swap = function X -> O | O -> X

type trace = (hpos * vpos) list [@@deriving show, eq]
type endplay = Win of player | Giveup of player [@@deriving eq]

let pp_endplay oc = function
  | Win p -> Format.fprintf oc "Win %a" pp_player p
  | Giveup p -> Format.fprintf oc "Giveup %a" pp_player p

type result = { trace : trace; endplay : endplay; final : board }

let arena ?(init_player = X) ?(init_board = Engine.empty) players =
  let open Lwt.Syntax in
  let rec go board player trace =
    let opponent = swap player in
    if win board opponent then
      Lwt.return
        { trace = List.rev trace; endplay = Win opponent; final = board }
    else
      let* r = players player board in
      match r with
      | None ->
          Lwt.return
            { trace = List.rev trace; endplay = Giveup player; final = board }
      | Some (i, j) ->
          let board = set board i j player in
          go board opponent ((i, j) :: trace)
  in
  go init_board init_player []

let player_giveup _board = Lwt.return None

let player_random board =
  let pos = free board in
  if List.length pos = 0 then Lwt.return None
  else Lwt.return (Some (List.nth pos (Random.int (List.length pos))))

let players_trace trace =
  let current_trace = ref trace in
  fun _player _board ->
    match !current_trace with
    | [] -> Lwt.return None
    | move :: trace ->
        current_trace := trace;
        Lwt.return (Some move)

let pair ~x:player_X ~o:player_O player =
  match player with X -> player_X | O -> player_O

let player_teletype player board =
  Format.printf "@[<v>Player %a to play.@," pp_player player;
  Format.printf "Board:  @[<v>%a@]@," pp_board board;
  Format.printf "Move?@]@.";
  try Scanf.scanf "%d %d\n" (fun i j -> Lwt.return (Some (Pos.h i, Pos.v j)))
  with Scanf.Scan_failure _ -> Lwt.return None
