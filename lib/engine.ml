type player = X | O [@@deriving show { with_path = false }, eq]

(* [hpos] and [vpos] are sealed off to avoid producing invalid
   positions. Only way to create indices is through the [Pos]
   module. *)

type hpos =
  | H of int  (** where [int] indicates a row number, between 0 and 2 *)
[@@deriving eq]

type vpos =
  | V of int  (** where [int] indicates a column number, between 0 and 2 *)
[@@deriving eq]

let int_of_hpos (H i) = i
let int_of_vpos (V i) = i

exception Invalid_hpos
exception Invalid_vpos

(* Setup a namespace for convenient short notations *)
module Pos = struct
  let h i =
    if i < 0 || i >= 3 then raise Invalid_hpos;
    H i

  let v j =
    if j < 0 || j >= 3 then raise Invalid_vpos;
    V j
end

(* Make sure pretty-printed notation can be parsed back by OCaml *)
let pp_hpos oc = function H i -> Format.fprintf oc "Pos.(h %d)" i
let pp_vpos oc = function V i -> Format.fprintf oc "Pos.(v %d)" i

type pos = hpos * vpos
type board = player option list list [@@deriving eq]

let init lb =
  assert (List.length lb = 3);
  assert (List.for_all (fun line -> List.length line = 3) lb);
  lb

let empty =
  let line = [ None; None; None ] in
  init [ line; line; line ]

let get b (H i) (V j) = List.nth (List.nth b i) j

let pp_board oc b =
  [ 0; 1; 2 ]
  |> List.iter (fun i ->
         [ 0; 1; 2 ]
         |> List.iter (fun j ->
                match get b (H i) (V j) with
                | None -> Format.fprintf oc "."
                | Some p -> Format.fprintf oc "%a" pp_player p);
         Format.fprintf oc "@,")

exception Invalid_move

let mapi k b =
  b |> List.mapi (fun i line -> line |> List.mapi (fun j v -> k (H i) (V j) v))

let iteri k b =
  b
  |> List.iteri (fun i line -> line |> List.iteri (fun j v -> k (H i) (V j) v))

let set b i j p =
  b
  |> mapi (fun si sj v ->
         if equal_hpos i si && equal_vpos j sj then
           match v with None -> Some p | _ -> raise Invalid_move
         else v)

let free b =
  let poss =
    [ 0; 1; 2 ]
    |> List.concat_map (fun i ->
           [ 0; 1; 2 ] |> List.map (fun j -> (Pos.h i, Pos.v j)))
  in
  poss
  |> List.filter (fun (i, j) ->
         match get b i j with None -> true | Some _ -> false)

let win b p =
  let diag_down = [ 0; 1; 2 ] |> List.map (fun i -> (H i, V i)) in
  let diag_up = [ 0; 1; 2 ] |> List.map (fun i -> (H i, V (2 - i))) in
  let lines =
    [ 0; 1; 2 ]
    |> List.map (fun i -> [ 0; 1; 2 ] |> List.map (fun j -> (H i, V j)))
  in
  let cols =
    [ 0; 1; 2 ]
    |> List.map (fun j -> [ 0; 1; 2 ] |> List.map (fun i -> (H i, V j)))
  in
  let pos = List.append (diag_up :: diag_down :: lines) cols in
  pos
  |> List.exists (fun patt ->
         patt |> List.for_all (fun (i, j) -> get b i j = Some p))
