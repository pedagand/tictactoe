type player = X | O

val pp_player : Format.formatter -> player -> unit
val equal_player : player -> player -> bool

type hpos
(** horizontal position (row) *)

type vpos
(** vertical position (column) *)

type pos = hpos * vpos

exception Invalid_hpos
exception Invalid_vpos

(** Smart position constructors *)
module Pos : sig
  val h : int -> hpos
  (** Convert an integer into an horizontal position. [raise
      Invalid_hpos] if the integer is out of [[0; 1; 2]] *)

  val v : int -> vpos
  (** Convert an integer into a vertical position. [raise
      Invalid_vpos] if the integer is out of [[0; 1; 2]] *)
end

val int_of_hpos : hpos -> int
val pp_hpos : Format.formatter -> hpos -> unit
val equal_hpos : hpos -> hpos -> bool
val int_of_vpos : vpos -> int
val pp_vpos : Format.formatter -> vpos -> unit
val equal_vpos : vpos -> vpos -> bool

type board
(** A 3-by-3 table, initially empty and monotonically filled by players. *)

val pp_board : Format.formatter -> board -> unit
val equal_board : board -> board -> bool

exception Invalid_move
(** Raised when a player attempts to overtake a pre-existing position *)

val init : player option list list -> board
(** [init pss] Initialize a board from a list of rows of list of columns. *)

val empty : board
(** [empty] board, with no position taken. *)

val get : board -> hpos -> vpos -> player option
(** [get board hpos vpos] queries the state of the [board] at the
    given position [(hpos, vpos)]. *)

val set : board -> hpos -> vpos -> player -> board
(** [set board hpos vpos player] puts down the [player]'s mark at the
    given position [(hpos, vpos)]. *)

val iteri : (hpos -> vpos -> player option -> unit) -> board -> unit
(** [iteri f board] iterates the function [f] over all positions of the [board]. *)

val free : board -> pos list
(** [free board] queries the list of all positions not taken by any player. *)

val win : board -> player -> bool
(** [win board player] returns [true] if and only if [player] has
    successfully aligned 3 positions (horizontally, vertically or
    diagonally). *)
