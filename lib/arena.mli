open Engine

type trace = (hpos * vpos) list
(** Game execution trace *)

val pp_trace : Format.formatter -> trace -> unit
val equal_trace : trace -> trace -> bool

(** Game outcome *)
type endplay = Win of player | Giveup of player

val pp_endplay : Format.formatter -> endplay -> unit
val equal_endplay : endplay -> endplay -> bool

type result = {
  trace : trace;  (** Trace *)
  endplay : endplay;  (** Outcome *)
  final : board;  (** Final state *)
}
(** Final state of the game *)

val arena :
  ?init_player:player ->
  ?init_board:board ->
  (player -> board -> (hpos * vpos) option Lwt.t) ->
  result Lwt.t
(** [arena ~init_player ~init_board players] simulates a game between
    [players X] and [players O]. By default, [init_player] is [X]
    while [init_board] is [empty]. *)

val players_trace : trace -> player -> board -> pos option Lwt.t
(** [players_trace trace] turns a (past) execution trace into a pair
    of [players]. Useful to replay a previous game. *)

val pair :
  x:(board -> pos option Lwt.t) ->
  o:(board -> pos option Lwt.t) ->
  player ->
  board ->
  pos option Lwt.t
(** [pair ~x:player_X ~o:player_O] combines two individual players
    [player_X] and [player_O] into an aggregated player, to pass to
    [arena]. *)

val player_teletype : player -> board -> pos option Lwt.t
(** [player_teletype player] delegates the choice of moves to the computer user, through terminal I/O. *)

val player_giveup : board -> pos option Lwt.t
(** [player_giveup] always fails to propose a move. *)

val player_random : board -> pos option Lwt.t
(** [player_random] produces random yet valid moves. *)
